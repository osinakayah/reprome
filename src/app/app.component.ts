import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { HomePage } from '../pages/home/home';
import { CounsellingPage } from '../pages/counselling/counselling';
import { InformationPage } from '../pages/information/information';
import { MorePage } from '../pages/more/more';
import {LoginPage} from "../pages/login/login";

import {VisitCenterPage} from "../pages/visit-center/visit-center";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private storage: Storage) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Counselling', component: CounsellingPage},
      { title: 'Visit a center', component: VisitCenterPage},
      { title: 'Information Hub', component: InformationPage},
      { title: 'More', component: MorePage},
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }
  logOut(){
    this.storage.clear();
    this.nav.setRoot(LoginPage);
  }
}
