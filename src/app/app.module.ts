import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { CounsellingPage } from '../pages/counselling/counselling';
import { InformationPage } from '../pages/information/information';
import { InformationHubDetailPage } from '../pages/information-hub-detail/information-hub-detail';
import { MorePage } from '../pages/more/more';
import { MoreDetailsPage } from '../pages/more-details/more-details';
import { LoginPage } from '../pages/login/login';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CallNumber } from '@ionic-native/call-number';
import { SMS } from '@ionic-native/sms';
import { ClinicsProvider } from '../providers/clinics/clinics';
import { AppointMentSheetComponent } from '../components/appoint-ment-sheet/appoint-ment-sheet';
import {RegistrationPage} from "../pages/registration/registration";
import {VisitCenterPage} from "../pages/visit-center/visit-center";
import {ShareStoryPage} from "../pages/share-story/share-story";
import {ContraceptivePage} from "../pages/contraceptive/contraceptive";
import {ContraceptiveDetailPage} from "../pages/contraceptive-detail/contraceptive-detail";
import { ContraceptiveProvider } from '../providers/contraceptive/contraceptive';
import {InAppBrowser} from "@ionic-native/in-app-browser";
import { AuthProvider } from '../providers/auth/auth';
import {StiPage} from "../pages/sti/sti";
import { StiProvider } from '../providers/sti/sti';
import { UtiProvider } from '../providers/uti/uti';
import {UtiPage} from "../pages/uti/uti";
import { PtiProvider } from '../providers/pti/pti';
import {PtiPage} from "../pages/pti/pti";
import {MomentModule} from "angular2-moment";
import {IonicStorageModule} from "@ionic/storage";
import {Network} from "@ionic-native/network";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    CounsellingPage,
    InformationPage,
    InformationHubDetailPage,
    MoreDetailsPage,
    MorePage,
    AppointMentSheetComponent,
    LoginPage,
    RegistrationPage,
    VisitCenterPage,
    ShareStoryPage,
    ContraceptivePage,
    ContraceptiveDetailPage,
    StiPage,
    UtiPage,
    PtiPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    MomentModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    CounsellingPage,
    InformationPage,
    InformationHubDetailPage,
    MoreDetailsPage,
    MorePage,
    AppointMentSheetComponent,
    LoginPage,
    RegistrationPage,
    VisitCenterPage,
    ShareStoryPage,
    ContraceptivePage,
    ContraceptiveDetailPage,
    StiPage,
    UtiPage,
    PtiPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
      CallNumber,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ClinicsProvider,
    SMS,
    ContraceptiveProvider,
    InAppBrowser,
    AuthProvider,
    StiProvider,
    UtiProvider,
    PtiProvider,
    Network
  ]
})
export class AppModule {}
