import { Component } from '@angular/core';
import {ModalController, NavParams, ToastController} from "ionic-angular";
import { SMS } from '@ionic-native/sms';
/**
 * Generated class for the AppointMentSheetComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'appoint-ment-sheet',
  templateUrl: 'appoint-ment-sheet.html'
})
export class AppointMentSheetComponent {

  appoinmentSheetTitle = "Appointment";
  appoinmentSheetLocation = "";
  phoneNumber: string;
  clientName: string = "Fullname";

  public event = {
    month: this.getStartDate(),
    timeStarts: '00:00',
    timeEnds: '2020-07-30'
  }

  constructor(public navParam: NavParams,
              public sms: SMS,
              public modalCtrl: ModalController,
              public toastCtrl: ToastController
  ) {
    this.appoinmentSheetLocation = navParam.get('location');
    this.phoneNumber =  navParam.get('number');
  }

  dismiss(){

  }

  sendMessage(){
    if(this.clientName == "" || this.clientName == 'Fullname'){
        this.presentToast('Please enter a name');
        return;
    }
    let  selectedDate = new Date(this.event.month);
    if(selectedDate.getTime() < new Date().getTime()){
      this.presentToast('Please enter a valid date');
      return;
    }
    if (this.clientName && this.clientName != '') {
      let messageBody = this.clientName+ ' booked an appointment for \n Date: '+ this.event.month+' '+this.event.timeStarts;

      this.sms.send(this.phoneNumber, messageBody, {replaceLineBreaks: true, android:{intent: 'INTENT'}});
    }

  }

  presentToast(message:string) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  getStartDate(){
    let todayDate = new Date();

    let day = todayDate.getDate();
    let month = todayDate.getMonth() + 1;
    let year = todayDate.getFullYear();

    return year+'-'+month+'-'+day;
  }


}
