import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {CounsellingPage } from '../counselling/counselling';
import { InformationPage }from  '../information/information';
import { MorePage } from '../more/more';
import {VisitCenterPage} from "../visit-center/visit-center";
import {ShareStoryPage} from "../share-story/share-story";
import {InAppBrowser} from "@ionic-native/in-app-browser";
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private inAppBrowser: InAppBrowser) {

  }
  gotoCounselling(){
    this.navCtrl.push(CounsellingPage);
  }

  gotoInformationHub(){
    this.navCtrl.push(InformationPage);
  }

  gotoMore(){
    this.navCtrl.push(MorePage);
  }

  gotoVisitCentre(){
    this.navCtrl.push(VisitCenterPage);
  }

  gotoShareStory(){
    this.navCtrl.push(ShareStoryPage);
  }

  openBrowser(){
    this.inAppBrowser.create("http://www.repromehub.com/contact");
  }

}
