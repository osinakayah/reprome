import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ContraceptiveProvider} from "../../providers/contraceptive/contraceptive";
import {VisitCenterPage} from "../visit-center/visit-center";
import {CallNumber} from "@ionic-native/call-number";

/**
 * Generated class for the ContraceptiveDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-contraceptive-detail',
  templateUrl: 'contraceptive-detail.html',
})
export class ContraceptiveDetailPage {
  id:any = 0;
  data = {};
  showMerit = false;
  showDemerit = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private contraceptiveProvider: ContraceptiveProvider, private call: CallNumber) {
    this.id = this.navParams.get('id');
  }

  ionViewDidLoad() {
    this.data = this.contraceptiveProvider.getContraceptive(this.id - 1);
  }

  toggleMerit(){
    this.showMerit = !this.showMerit;
  }

  toggleDemerit(){
    this.showDemerit = !this.showDemerit;
  }
  callNumber(){
    this.call.callNumber('22252', true);
  }
  visitCenter(){
    this.navCtrl.push(VisitCenterPage);
  }
}
