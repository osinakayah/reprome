import { Component } from '@angular/core';
import {LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {RegistrationPage} from "../registration/registration";
import {HomePage} from "../home/home";
import {AuthProvider} from "../../providers/auth/auth";
import { Storage } from '@ionic/storage';
import {Network} from "@ionic-native/network";

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginDetails:any = {};
  loading:any = null;
  isConnected = false;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController, private auth: AuthProvider,
              private loadingCtrl: LoadingController,
              private storage: Storage,
              private network: Network) {

  }


  ionViewDidLoad() {
    if(this.network.type === 'none'){
      this.isConnected = false;
    }else{
      this.isConnected = true;
    }

    this.network.onDisconnect().subscribe(() => {
      this.isConnected = false;
      console.log(0);
    });

    this.network.onConnect().subscribe(() => {
      this.isConnected = true;
      console.log(1);
    });
    
    this.storage.get('email').then((res)=>{
        if(res){
          this.navCtrl.setRoot(HomePage)
        }
    });
  }

  gotoRegistrationPage(){
    this.navCtrl.push(RegistrationPage);
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });

    toast.present();
  }

  presentLoader(){
    this.loading = this.loadingCtrl.create({
      content:'Please wait...'
    });

    this.loading.present();
  }
  dismissLoader(){
    if(this.loading != null){
      this.loading.dismiss();
    }
  }

  loginUser(){
      if(!this.isConnected){
        this.presentToast('No Network');
        return;
      }

      this.presentLoader();
      this.auth.login(this.loginDetails.email, this.loginDetails.password).then((res:any)=>{
        this.dismissLoader();
        if(res.status == 'ok'){
          this.storage.set('email', res.email);
          this.navCtrl.setRoot(HomePage);
          this.presentToast('Login Successfully');
        }else{
          this.presentToast('Wrong username or password');
        }
      }, ()=>{
        this.dismissLoader();
        this.presentToast('Oops, an error occured');
      });
    
  }
}
