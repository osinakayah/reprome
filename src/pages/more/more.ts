import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MoreDetailsPage } from '../more-details/more-details';

/**
 * Generated class for the MorePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-more',
  templateUrl: 'more.html',
})
export class MorePage {
  faq: any = [
    {title: 'How Does a Girl Know She Has Chlamydia?', answer: 'It can be difficult for a girl to know whether she has chlamydia because most girls don\'t have any symptoms. Because of this, it\'s very important to see a doctor and get tested for chlamydia at least once a year if you are having vaginal, oral, or anal sex. Your doctor can tell you about how to test for chlamydia, even if you don\'t have any symptoms.'},
    {title: 'How Does a Guy Know She Has Chlamydia?', answer: `It also can be difficult for guys to know if they have chlamydia. Many who do have it will have few or no symptoms, so any guy who is having vaginal, oral, or anal sex should be tested by a doctor at least once a year.
When symptoms are there, guys may have a discharge from the tip of the penis (the urethra — where urine comes out), or itching or burning sensations around the penis. Rarely, one of the testicles may become swollen.`},
    {title: 'When does Chlamydia symptoms appear', answer: 'Someone who has chlamydia may see symptoms a week later. In some people, the symptoms take up to 3 weeks to appear, and many people never develop any symptoms.'},
    {title: 'What Are the Symptoms of Hepatitis', answer: `Someone infected with hepatitis B also might have abdominal (belly) pain or pain underneath the right ribcage where the liver is.
Hepatitis B can cause jaundice, (pronounced: JAWN-diss) a yellowing of the skin and the whites of the eyes. Jaundice also can make urine (pee) look brownish.
Many people infected with hepatitis B do not have any symptoms until later on. At that stage, a person can have more serious complications, such as liver damage.`},
    {title: 'How Long Until Symptoms of Hepatitis Appear', answer: `Someone who has been exposed to hepatitis B may start to have symptoms any time from 1 to 6 months later. Symptoms can last for weeks to months. Some people with hepatitis B don't notice symptoms until they become quite severe. Some have few or no symptoms, but even someone who doesn't have any symptoms can still spread the disease to others, and can still develop complications later in life.`},
    {title: 'What Are the Signs of Gonorrhea in Girls', answer: `A girl who has gonorrhea may have no symptoms at all or her symptoms may be so mild that she doesn't notice them until they become more severe. In some cases, girls will feel a burning sensation when they pee, or they will have a yellow-green vaginal discharge. Girls also may have vaginal bleeding between menstrual periods.`},
    {title: 'What Are the Signs of Gonorrhea in Guys?', answer: `Guys who have gonorrhea are much more likely to notice symptoms, although a guy can have gonorrhea and not know it. Guys often feel a burning sensation when they pee, and yellowish-white discharge may ooze out of the urethra (at the tip of the penis).`},
    {title: 'How Is Gonorrhea Treated?', answer: `If you have gonorrhea, your doctor will prescribe antibiotics to treat the infection. Any sexual partners should also be tested and treated for gonorrhea immediately. This includes any partners in the last 2 months, or your last sexual partner if it has been more than 2 months since you last had sex.
If a sexual partner has gonorrhea, quick treatment will reduce the risk of complications for that person and will lower your chances of being reinfected if you have sex with that partner again. (You can become infected with gonorrhea again even after treatment — having it once doesn't make you immune to it.)`},

  ];

  usefulLinks: any = [
    {title:'Reprome', URL:'http://www.repromehub.com'},
    {title:'Marie Stopes', URL:'https://www.mariestopes.org/'}
  ];


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MorePage');
  }

  itemSelected(id: number){

    if(id === 0){
      this.navCtrl.push(MoreDetailsPage, {type: 0, data: this.faq});
    }
    else if(id === 2){
      this.navCtrl.push(MoreDetailsPage, {type: 2, data: this.usefulLinks});
    }
    else if(id === 1) {
      this.navCtrl.push(MoreDetailsPage, {type: 1, data: null});
    }
    else if(id === 4){
      this.navCtrl.push(MoreDetailsPage, {type: 4, data: ''});
    }

  }

}
