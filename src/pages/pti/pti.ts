import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {VisitCenterPage} from "../visit-center/visit-center";
import {CallNumber} from "@ionic-native/call-number";
import {PtiProvider} from "../../providers/pti/pti";

/**
 * Generated class for the PtiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-pti',
  templateUrl: 'pti.html',
})
export class PtiPage {
  selectedContent = -1;
  information:any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private call: CallNumber, private pdiProvider: PtiProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PtiPage');
    this.information = this.pdiProvider.getPDI();
  }

  toggleView(id:number){
    if(this.selectedContent == id){
      this.selectedContent = -1;
    }else{
      this.selectedContent = id;
    }
  }

  callNumber(){
    this.call.callNumber('22252', true);
  }
  visitCenter(){
    this.navCtrl.push(VisitCenterPage);
  }

}
