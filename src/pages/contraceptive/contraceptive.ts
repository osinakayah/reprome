import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ContraceptiveDetailPage} from "../contraceptive-detail/contraceptive-detail";

/**
 * Generated class for the ContraceptivePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-contraceptive',
  templateUrl: 'contraceptive.html',
})
export class ContraceptivePage {
  condom = false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContraceptivePage');
  }

  gotoNext(id: any){
    this.condom = !this.condom;
    if(id == 0){
      return;
    }
    this.navCtrl.push(ContraceptiveDetailPage, {id: id});

  }

}
