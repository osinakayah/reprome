import { Component } from '@angular/core';
import {LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {HomePage} from "../home/home";
import {AuthProvider} from "../../providers/auth/auth";
import {Network} from "@ionic-native/network";
import { Storage } from '@ionic/storage';

/**
 * Generated class for the RegistrationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
  registrationDetails: any = {};
  loading:any = null;
  isConnected = false;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private authProvider: AuthProvider,
              private storage: Storage,
              private network: Network) {
  }

  ionViewDidLoad() {
    if(this.network.type === 'none'){
      this.isConnected = false;
    }else{
      this.isConnected = true;
    }

    this.network.onDisconnect().subscribe(() => {
      this.isConnected = false;
    });

    this.network.onConnect().subscribe(() => {
      this.isConnected = true;
    });
  }

  presentLoader(){
    this.loading = this.loadingCtrl.create({
      content:'Please wait...'
    });

    this.loading.present();
  }
  dismissLoader(){
    if(this.loading != null){
      this.loading.dismiss();
    }
  }

  registerUserWithDetails(){
      if(!this.isConnected){
        this.presentToast('No network');
        return;
      }
      if(this.registrationDetails.cpassword == this.registrationDetails.password){
        if(this.registrationDetails.email) {
          this.presentLoader();
          this.authProvider.register(this.registrationDetails.name, this.registrationDetails.email, this.registrationDetails.password).then((res:any)=>{
            this.dismissLoader();
            if(res.status == 'ok'){
              this.storage.set('email', res.email);
              this.presentToast('Registered Successfully');
              this.navCtrl.setRoot(HomePage);
            }else{
              this.presentToast('Your email or phone number has been used before');
            }
          }, ()=>{
            this.dismissLoader();
            this.presentToast('Your email or phone number has been used before');
          });
        }
      }
      else{
        this.presentToast('Password and username does not match');
      }
  }

  backToLogin(){
    this.navCtrl.pop();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });

    toast.present();
  }

}
