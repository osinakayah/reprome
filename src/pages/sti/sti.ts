import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {StiProvider} from "../../providers/sti/sti";
import {CallNumber} from "@ionic-native/call-number";
import {VisitCenterPage} from "../visit-center/visit-center";

/**
 * Generated class for the StiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-sti',
  templateUrl: 'sti.html',
})
export class StiPage {
  selectedContent = -1;
  information: any = [];
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private stiProvider: StiProvider,
              private call: CallNumber
  ) {
    this.information = this.stiProvider.getAllSTI();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StiPage');
  }
  toggleView(id:number){
    if(this.selectedContent == id){
      this.selectedContent = -1;
    }else{
      this.selectedContent = id;
    }
  }

  callNumber(){
    this.call.callNumber('22252', true);
  }
  visitCenter(){
    this.navCtrl.push(VisitCenterPage);
  }

}
