import { Component } from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ClinicsProvider} from "../../providers/clinics/clinics";
import {AppointMentSheetComponent} from "../../components/appoint-ment-sheet/appoint-ment-sheet";

/**
 * Generated class for the VisitCenterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-visit-center',
  templateUrl: 'visit-center.html',
})
export class VisitCenterPage {
  clinics: any = [];
  availableClinics = [];
  states: any = '';


  clinicQueryString = '';
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private clinicsProvider: ClinicsProvider,
              private alertCtrl: AlertController,
              private modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VisitCenterPage');
    this.clinics = this.clinicsProvider.getClinics();
  }
  openAppointmentSheet(clinicName: string, clinicLocation: string, clinicNumber: string) {
    let appointmentSheet = this.modalCtrl.create(AppointMentSheetComponent, {name: clinicName, location: clinicLocation, number: clinicNumber});
    appointmentSheet.present();
  }
  openClinicModal(clinicLocation: string, clinicName: string, clinicNumber: string){
    let confirmAlertDialog = this.alertCtrl.create({
      title: clinicName,
      message: clinicName+' is located at '+ clinicLocation+'. Do you want to book an appointment',
      buttons: [
        {
          text:'Cancel',
          handler: ()=>{console.log('Cancel');}
        },
        {
          text: 'Book appointment',
          handler: ()=>{
            this.openAppointmentSheet(clinicName, clinicLocation, clinicNumber);
          }
        }
      ]
    });
    confirmAlertDialog.present();
  }

  onSearch(event: Event) {
    this.clinicQueryString = (<HTMLInputElement>event.target).value;
    this.availableClinics = [];
    for(let q = 0; q < this.clinics.length; q++ ){
      let state = this.clinics[q];
      if(this.states == state.state){
        this.availableClinics = [];
        for(let w = 0; w < state.LGA.length; w++ ){
          let LGA = state.LGA[w];
          switch (this.states){
            case 'Abia':
              this.availableClinics.push({name: 'Marie Stopes Edo', location:'No 2 Arousa Street, off Sakponba road, Benin City Edo State', doctor: 'DR MAGNUS ODIDO', number: '08077363666'});
              break;
            case 'Abuja':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Adamawa':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Akwa Ibom':
              this.availableClinics.push({name: 'Marie Stopes Edo', location:'No 2 Arousa Street, off Sakponba road, Benin City Edo State', doctor: 'DR MAGNUS ODIDO', number: '08077363666'});
              break;
            case 'Anambra':
              this.availableClinics.push({name: 'Marie Stopes Edo', location:'No 2 Arousa Street, off Sakponba road, Benin City Edo State', doctor: 'DR MAGNUS ODIDO', number: '08077363666'});
              break;
            case 'Bauchi':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Benue':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Borno':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Cross River':
              this.availableClinics.push({name: 'Marie Stopes Edo', location:'No 2 Arousa Street, off Sakponba road, Benin City Edo State', doctor: 'DR MAGNUS ODIDO', number: '08077363666'});
              break;
            case 'Delta':
              this.availableClinics.push({name: 'Marie Stopes Edo', location:'No 2 Arousa Street, off Sakponba road, Benin City Edo State', doctor: 'DR MAGNUS ODIDO', number: '08077363666'});
              break;
            case 'Ebonyi':
              this.availableClinics.push({name: 'Marie Stopes Edo', location:'No 2 Arousa Street, off Sakponba road, Benin City Edo State', doctor: 'DR MAGNUS ODIDO', number: '08077363666'});
              break;
            case 'Edo':
              this.availableClinics.push({name: 'Marie Stopes Edo', location:'No 2 Arousa Street, off Sakponba road, Benin City Edo State', doctor: 'DR MAGNUS ODIDO', number: '08077363666'});
              break;
            case 'Ekiti':
              this.availableClinics.push({name: 'Marie Stopes Edo', location:'No 2 Arousa Street, off Sakponba road, Benin City Edo State', doctor: 'DR MAGNUS ODIDO', number: '08077363666'});
              break;
            case 'Enugu':
              this.availableClinics.push({name: 'Marie Stopes Edo', location:'No 2 Arousa Street, off Sakponba road, Benin City Edo State', doctor: 'DR MAGNUS ODIDO', number: '08077363666'});
              break;
            case 'Gombe':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Imo':
              this.availableClinics.push({name: 'Marie Stopes Edo', location:'No 2 Arousa Street, off Sakponba road, Benin City Edo State', doctor: 'DR MAGNUS ODIDO', number: '08077363666'});
              break;
            case 'Jigawa':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;


            case 'Kaduna':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Kano':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Kastina':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;

            case 'Kebbi':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Kogi':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Kwara':
              this.availableClinics.push({name: 'Marie Stopes Lagos', location:'No 2 Araromi compound, Araromi bus stop, Lasu-Isheri road, Akesan Igando, Lagos State', doctor: 'DR MAGNUS ODIDO', number: '08077443107'});
              break;
            case 'Lagos':
              this.availableClinics.push({name: 'Marie Stopes Lagos', location:'No 2 Araromi compound, Araromi bus stop, Lasu-Isheri road, Akesan Igando, Lagos State', doctor: 'DR MAGNUS ODIDO', number: '08077443107'});
              break;
            case 'Nasarawa':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Niger':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Ogun':
              this.availableClinics.push({name: 'Marie Stopes Lagos', location:'No 2 Araromi compound, Araromi bus stop, Lasu-Isheri road, Akesan Igando, Lagos State', doctor: 'DR MAGNUS ODIDO', number: '08077443107'});
              break;
            case 'Ondo':
              this.availableClinics.push({name: 'Marie Stopes Lagos', location:'No 2 Araromi compound, Araromi bus stop, Lasu-Isheri road, Akesan Igando, Lagos State', doctor: 'DR MAGNUS ODIDO', number: '08077443107'});
              break;
            case 'Osun':
              this.availableClinics.push({name: 'Marie Stopes Lagos', location:'No 2 Araromi compound, Araromi bus stop, Lasu-Isheri road, Akesan Igando, Lagos State', doctor: 'DR MAGNUS ODIDO', number: '08077443107'});
              break;
            case 'Oyo':
              this.availableClinics.push({name: 'Marie Stopes Lagos', location:'No 2 Araromi compound, Araromi bus stop, Lasu-Isheri road, Akesan Igando, Lagos State', doctor: 'DR MAGNUS ODIDO', number: '08077443107'});
              break;
            case 'Plateau':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Rivers':
              this.availableClinics.push({name: 'Marie Stopes Edo', location:'No 2 Arousa Street, off Sakponba road, Benin City Edo State', doctor: 'DR MAGNUS ODIDO', number: '08077363666'});
              break;
            case 'Sokoto':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Taraba':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Yobe':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
            case 'Zamfara':
              this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
              break;
          }
          if(LGA.name.toLowerCase().includes(this.clinicQueryString.toLowerCase())){
            for(let e = 0; e < LGA.clinics.length; e++ ){
              this.availableClinics.push(LGA.clinics[e]);
            }
          }
        }
        break;
      }
      else {
        this.availableClinics = [];
        this.availableClinics.push({name: 'MARIE STOPES CLINIC', location:'No 29 Ekukinam Street, Opp Alobiro Building, along utako market road, utako district abuja', doctor: 'DR MAGNUS ODIDO', number: '08170416263'});
      }

    }
  }

}
