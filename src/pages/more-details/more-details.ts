import { Component } from '@angular/core';
import {LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {VisitCenterPage} from "../visit-center/visit-center";
import {CallNumber} from "@ionic-native/call-number";
import {AuthProvider} from "../../providers/auth/auth";
import {Network} from "@ionic-native/network";

/**
 * Generated class for the MoreDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-more-details',
  templateUrl: 'more-details.html',
})
export class MoreDetailsPage {
  loading:any = null;
  isConnected = false;
  isFaq: boolean = false;
  isFeedback: boolean = false;
  isUsefulLinks: boolean = false;
  isRecandSugget: boolean = false;
  data: any;
  title:string;
  recommendationFullname: string;
  recommendationMessage: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private inAppBrowser: InAppBrowser,
              private call: CallNumber,
              private auth: AuthProvider,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private network: Network) {

    this.isFaq = false;
    this.isFeedback = false;
    this.isUsefulLinks = false;
    let type = navParams.get('type');
    this.data = navParams.get('data');

    if(type == 0){
      this.isFaq = true;
      this.title = 'FAQ';
    }
    else if(type == 2) {
      this.isUsefulLinks = true;
      this.title = 'Useful links';
    }
    else if(type == 1) {
      this.isFeedback = true;
      this.title = 'Feedback';
    }
    else if(type == 4){
      this.isRecandSugget = true;
      this.title = 'Recommendation and Suggestions';
    }
  }

  ionViewDidLoad() {
    this.recommendationFullname = "";
    this.recommendationMessage = "";
    if(this.network.type === 'none'){
      this.isConnected = false;
    }else{
      this.isConnected = true;
    }

    this.network.onDisconnect().subscribe(() => {
      this.isConnected = false;
      console.log(0);
    });

    this.network.onConnect().subscribe(() => {
      this.isConnected = true;
      console.log(1);
    });
  }

  openBrowser(url){
    this.inAppBrowser.create(url);
  }

  callNumber(){
    this.call.callNumber('22252', true);
  }
  visitCenter(){
    this.navCtrl.push(VisitCenterPage);
  }

  sendRecommendation(){
    if(!this.isConnected){
      this.presentToast('No Network');
      return;
    }
    this.presentLoader();
    this.auth.sendReommendation(this.recommendationFullname, this.recommendationMessage).then((res)=>{
      this.dismissLoader();
      this.presentToast("Recommendation Sent");
    }, ()=>{
      this.dismissLoader();
      this.presentToast("Recommendation Sent");
    })
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });

    toast.present();
  }

  presentLoader(){
    this.loading = this.loadingCtrl.create({
      content:'Please wait...'
    });

    this.loading.present();
  }
  dismissLoader(){
    if(this.loading != null){
      this.loading.dismiss();
    }
  }
}
