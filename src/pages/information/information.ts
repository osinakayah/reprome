import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InformationHubDetailPage } from '../information-hub-detail/information-hub-detail'

/**
 * Generated class for the InformationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-information',
  templateUrl: 'information.html',
})
export class InformationPage {
  informations: any = [
    {id: 'Ladies Business', data:[
          {title:'My period cycle', body:`Also known as the Menstrual cycle is counted from the first day of your period to the first day of the next period. The average menstrual cycle is 28 days long. Cycles can range from 21 to 35 days in adult and 21 to 45 days in teens and adolescent. The cycle comprises of two distinct phases, the safe (a period when an unprotected sex will not result to a pregnancy and unsafe periods (a period where any act of unprotected sex might in pregnancy). 
          
          <h2>Facts</h2>
          <ul>
            <li>Ovulation is the process when hormone changes trigger an ovary to release an egg.</li>
            <li>You can only become pregnant if a sperm meets with and fertilises an egg.</li>
            <li>Ovulation normally occurs 24 to 36 hours after the luteinizing hormone (LH) surge, so identifying this LH surge helps you to determine when you are about to ovulate.</li>
            <li>Sperm can remain active for up to five days, so couples can conceive by having intercourse prior to the egg being released.</li>
         
          </ul>

          `},
          {title:'What is Ovulation', body:`Ovulation is the name of the process that happens usually once in every menstrual cycle, when hormonal changes trigger an ovary to release an egg. Ovulation usually happens 12-16 days before the next period. Many women think that they ovulate on day 14 but this is just an average. Most women will actually ovulate on a different day of the menstrual cycle, and this will also vary from cycle to cycle. 
          Some women claim to feel a twinge of pain when they ovulate but many feel no sensation at all and there are no other physical signs of ovulation. To avoid getting pregnant, it’s important to have intercourse on your safe days. 
          `},
          {title:'Period calendars', body:`An ovulation calendar or calculator is an online tool that attempts to predict when you might ovulate based on the length of your menstrual cycles. Maybe you’re looking for an ovulation calendar to help you time sex to plan or avoid pregnancy. Unfortunately, these kinds of calendars and calculators are not accurate.
          `},{title:'How Does It Work?', body:`The most basic ovulation calendar will ask you for the date of the first day of your last period, and the average length of your menstrual cycles. If you don’t know, most calendars will suggest you write in 28 days. This is considered the average. (A normal cycle can range from 21 to 35 days.)
          Then, the calculator will usually assume a luteal phase of 14. The luteal phase is the time between ovulation and the first day of your period. A normal luteal phase can be as short as 10 days or as long as 15. 
          Next, based on this information, the ovulation calculator will make a guess at what days you are most likely to be fertile and perhaps the day you may ovulate. 
          `},{title:'Facts', body:`There is no such thing as an “accurate” ovulation calendar or calculator. By definition, they are based on averages. What you want in an ovulation calendar isn’t an accurate, specific date but a nice generous wide range. That way, you don’t need to worry about the calculator telling you you’re fertile too early or too late. You’ll cover all your bases.
          `},
          {title: 'Menstral hygiene', body:`What happens during menstruation
            During menstruation, an unfertilized egg breaks down dropping the hormone levels which triggers the shedding of the thickened lining of the uterus called menstruation. This flow might be followed with some cramping and other premenstrual syndrome in some people. This blood flow needs to be properly cleaned up to avoid embarrassment and prevent infections. This can be achieved by maintain a proper hygiene during your flow days. To attain a good hygiene level, you should be able to:
            <ul>
              <li>Change your pad/tampon before it becomes soaked or change every 4-8 hours depending on the flow level.</li>
              <li>Wash the moon spot (without soap) every time you change your pad/tampon or urinate</li>
              <li>Avoid unprotected sex </li>
              <li>Change undies alongside pad/tampon</li>
              <li>Take a bath or shower at least once a day</li>
            </ul>
            <h2>Facts</h2>
            Menstrual hygiene is necessary in the prevention and spread of STIs and UTIs. A simple technique of washing and changing pads/tampons regularly can contribute to a woman’s health and wellbeing.
          `}
        ]
    },
    {id: 'Contraceptive', data:[
          {title: 'Pregnancy prevention in adolescent', body: `Pregnancy in adolescents which is a global challenge is a socially, economically, physically and emotionally complex issue. It has been reported that approximately 5 out of 20 girls get pregnant before the age of 20. This often occurs because they are not equipped with the right information in taking charge of our bodies and health as young adults. We believe most pregnant teens do not wish or never planned for the occurrence and thus ways of preventing teen pregnancy should be a high priority. There are three effective ways of preventing pregnancy which is either through practice of abstinence, the continuous use of a contraception method and the use of an effective emergency contraceptive.`},
          {title: 'Abstinence', body: `Abstinence from sex (oral, anal or vaginal) is the only behavior that is 100 percent effective at preventing teenage pregnancy. In fact, the National Campaign stresses that abstinence from sex is the best choice for teens as it avoids early pregnancy, parenthood and sexually transmitted infections (STIs). it is believed that teens remaining abstinent is a behavior choice and it can be difficult for some people.`}
        ]
    },
    {id: 'Infection and Diseases', data:[
          {title:'STD\'s', body:`
          Sexually transmitted diseases (STDs) are infectious diseases that spread from person to person through sex. STDs can affect guys and girls of all ages who are having sex. Unfortunately, STDs (sometimes also called STIs for "sexually transmitted infections") have become common among teens, adolescents and young adults. Because teens, adolescent and young adults are more at risk for getting some STDs, it's important to learn what you can do to protect yourself.
          STDs are more than just an embarrassment If untreated, some STDs can cause permanent damage, such as infertility (the inability to have a baby) in future and even death (in the case of HIV/AIDS).
          <h2>How STD Spread</h2>
          STDs spread easily because you can't tell whether someone has an infection. In fact, some people with STDs don't even know that they have them. These people are in danger of passing an infection on to their sex partners without even realizing it.
          Some of the things that increase a person's chances of getting an STD are:
          <ul>
            <li><b>Sexual activity at a young age.</b> The younger a person starts having sex, the greater his or her chances of becoming infected with an STD</li>
            <li><b>Lots of sex partners.</b> People who have sexual contact — not just intercourse, but any form of intimate activity — with many different partners are more at risk than those who stay with the same partner.</li>
            <li><b>Unprotected sex.</b> Latex condoms are the only form of birth control that reduce your risk of getting an STD, and must be used every time. Spermicides, diaphragms, and other birth control methods may help prevent pregnancy, but they don't protect a person against STDs.</li>
          </ul>
          <h2>Common STD symptoms in women</h2>
          <ul>
            <li>No symptoms</li>
            <li>Discharge (thick or thin, milky white, yellow, or green leakage from the vagina) </li>
            <li>Vagina itching</li>
            <li>Vaginal blisters or blisters in the genital area (the region covered by underwear)</li>
            <li>area (the region covered by underwear)</li>
            <li>Burning urination</li>
            <li>Painful urination</li>
            <li>Pain during intercourse</li>
          </ul>
          <h2>Less common STD symptoms in women:</h2>
          <ul>
            <li>Bleeding or spotting between menstrual cycles</li>
            <li>Painless ulcers on the vagina</li>
            <li>Pelvic pain</li>
            <li>Lower back pain</li>
            <li>Fever</li>
            <li>Nausea</li>
            <li>Sore throat (after oral sex)</li>
            <li>Swelling of the joints (knee, elbow, etc.)</li>
            <li>Rectal pain, bleeding, or discharge (after receiving anal sex)</li>
          </ul>
          When symptoms do occur, they typically appear within days or weeks of exposure to an STD. Often, symptoms never appear or go unnoticed. Even if an infection never results in obvious symptoms, the STD can still be transmitted and progress into a more serious condition that may result in irreversible side effects. Regular comprehensive STD testing is the only way to guarantee a clean bill of sexual health. It is especially important to get tested for STDs after risky or unprotected sexual contact. 
          <h2>Common STD symptoms in men</h2>
          <ul>
            <li>Being asymptomatic or experiencing no symptoms at all</li>
            <li>Blisters on or around penis</li>
            <li>Stops, bumps or lesions on the penis</li>
            <li>Discharge (clear, white, or yellow)</li>
            <li>Oozing from the tip of the penis (thick or thin)</li>
            <li>Painful urination</li>
            <li>Painful ejaculation</li>
            <li>Itching on the tip of the penis</li>
            <li>Rash on the penis, testicles, or groin</li>
          </ul>
          <h2>Less common STD symptoms in men:</h2>
          <ul>
            <li>Sore throat</li>
            <li>Fever</li>
            <li>Chronic flu-like symptoms</li>
            <li>Pain in the testicles</li>
            <li>Swelling of the testicles</li>
            <li>Swelling of the epididymis (known as Epididymitis)</li>
            <li>Swelling of the urethra (known as urethritis)</li>
            <li>Swelling of non-sexual joints (elbow, knee, etc.)</li>
            <li>Rectal pain, bleeding, or discharge (after receiving anal sex)</li>
          </ul>
          STD symptoms in men usually take a few days to develop, but can take up to weeks (if there are symptoms at all). A lack of symptoms is often mistaken for a lack of an STD, but an infection can continue to progress even in the absence of symptoms. Because men so often don’t show symptoms, the only way to be sure that an STD is not present is to get tested regularly, especially after unprotected sex. 
If you think you may have one or more of the symptoms or worried you might have been exposed to the infection or disthrough sex, please visit any of the health centres listed in our contact for comprehensive testing and treatment.
          <h2>Preventing and treating STDs</h2>
          As with many other diseases, prevention is key. It's much easier to prevent STDs than to treat them. The only way to completely prevent STDs is to abstain from all types of sexual contact. If someone is going to have sex, the best way to reduce the chance of getting an STD is by using a condom every time 
          People who have had or are having sex should get regular testing and screening examinations available at in all health centres listed in our contact. This is to help you get correct information on STDs and access services when they're detected in their earliest, most treatable stage.
          Don't let embarrassment at the thought of having an STD keep you from seeking medical attention or treatment as doing so may allow the disease to progress and cause more damage. Book appointments using the app appointment button to see a doctor If you think you may have an STD, or if you have had a partner who may have an STD, you should see a doctor right away.
          <h3>Common types of STDs common among teens, adolescent and young adults include:</h3>
          <hr>
          <h2>Chlamydia</h2>
          Chlamydia is a sexually transmitted disease (STD) caused by bacteria called Chlamydia trachomatis. Although you may not be familiar with its name, chlamydia is one of the most common STDs. Because there often aren't any symptoms, though, lots of people can have chlamydia and not know it. The bacteria can move from one person to another through vaginal, oral, or anal sex
          If you think you may have chlamydia or you might have been exposed to the bacteria through sex, please visit any of the health centres listed in our contact for comprehensive testing and treatment.  
          <h2>Hepatitis</h2>
          Hepatitis is a disease of the liver. It is usually caused by a virus, but also can be caused by long-term overuse of alcohol or other toxins (poisons).
          There are several different types of hepatitis. Hepatitis B is a type that can move from one person to another through blood and other body fluids. People also can get it through having sex or from needles that haven't been properly sterilized. 
          If you think you may have hepatitis B or you might have been exposed to the virus through sex or any other means visit any of the health centres listed in our contact for comprehensive testing and treatment. The good news is HB can be treated completely.
          <h2>Gonorrhea</h2>
          Gonorrhea is a sexually transmitted disease caused by bacteria. The bacteria can be passed from one person to another through vaginal, oral, or anal sex, even when the person who is infected has no symptoms.
          If you think you may have hepatitis B or you might have been exposed to the bacteria through sex or any other means visit any of the health centres listed in our contact for comprehensive testing and treatment. The good news is HB can be treated completely
          <hr>
          <h2>HIV</h2>
          The human immunodeficiency virus (HIV) is one of the most serious, deadly diseases in human history. HIV causes a condition called acquired immunodeficiency syndrome better known as AIDS.
          Thousands of young adults get infected with HIV each year. HIV can be transmitted from an infected person to another person through body fluids like blood, semen, vaginal fluids, and breast milk.
          The virus is spread through things like:
           <ul>
            <li>having unprotected oral, vaginal, or anal sex ("unprotected" means not using a condom)</li>
            <li>sharing needles, such as needles used to inject drugs, steroids, and other substances, or sharing needles used for tattooing including other risk factors</li>
           </ul>
           If you think you may have HIV or you might have been exposed to the virus through sex or any other means visit any of the health centres listed in our contact for comprehensive testing and treatment. The good news is HB can be treated completely
          `},
          {title: 'PID', body:`Pelvic inflammatory disease (PID) is an infection of the fallopian tubes, uterus, or ovaries. Most girls with PID develop it after getting a sexually transmitted disease (STD), such as chlamydia or gonorrhea.
            Girls who have sex with different partners or don't use condoms are most likely to get STDs and be at risk for PID. If PID is not treated, it can lead to internal scarring that might cause ongoing pelvic pain, infertility, or an ectopic pregnancy.
            If you think you may have hepatitis B or you might have been exposed to the bacteria through sex or any other means visit any of the health centres listed in our contact for comprehensive testing and treatment. The good news is HB can be treated completely.
            <h2>What are the symtoms PID</h2>
             PID can cause severe symptoms or very mild to no symptoms. Girls who do have symptoms may notice:
             <ul>
               <l1>Pain and tenderness in the lower abdomen</l1>
               <li>Bad-smelling or abnormally colored discharge</li>
               <li>Pain during sex</li>
               <li>Spotting (small amounts of bleeding) between periods</li>
               <li>Chills or fever</li>
               <li>nausea, vomiting, or diarrhea</li>
               <li>loss of appetite</li>
               <li>backache and perhaps even difficulty walking</li>
               <li>pain while peeing or peeing more often than usual</li>
               <li>pain in the upper abdomen on the right</li>
             </ul>
            `},
          {title: 'UTI', body:`A bacterial urinary tract infection (UTI) is the most common kind of infection affecting the urinary tract. Urine, or pee, is the fluid that is filtered out of the bloodstream by the kidneys. Urine contains salts and waste products, but it doesn't normally contain bacteria. When bacteria get into the bladder or kidney and multiply in the urine, a UTI can result.
            There are three main types of UTI. Bacteria that infect only the urethra (the short tube that delivers urine from the bladder to the outside of the body) cause urethritis (pronounced: yur-ih-threye-tis).
            Bacteria can also cause a bladder infection, which is called cystitis (pronounced: sis-tie-tis). Another, more serious, kind of UTI is infection of the kidney itself, known as pyelonephritis (pronounced: pie-low-nih-fry-tis). With this type of UTI, a person often has back pain, high fever, and vomiting.The most common type of UTI, the bladder infection, causes mostly just discomfort and inconvenience. Bladder infections can be quickly and easily treated. And it's important to get treatment promptly to avoid the more serious infection that reaches the kidneys.
            <h2>What Causes UTI</h2>
            UTIs are usually caused by E. coli, bacteria that are normally found in the digestive tract and on the skin around the rectal and vaginal areas. When the bacteria enter the urethra, they can make their way up into the bladder and cause an infection.
            Girls get urinary tract infections much more frequently than guys, most likely due to differences in the shape and length of the urethra. Girls have shorter urethras than guys, and the opening lies closer to the rectum and vagina where bacteria are likely to be.
            Bacteria can get into the urethra several ways. During sexual intercourse, for example, the bacteria in the vaginal area may be pushed into the urethra and eventually end up in the bladder, where urine provides a good environment for the bacteria to grow. This is the reason why females who are sexually active often get UTIs (UTIs are not contagious, so you can't catch a urinary tract infection from someone else).
            Bacteria may also be introduced into a girl's bladder by wiping from back to front after a bowel movement, which can contaminate the urethral opening. The use of spermicides (including condoms treated with spermicide) and diaphragms as contraceptives also may increase the risk of UTIs.
            Sexually transmitted diseases (STDs) may cause UTI-like symptoms, such as pain with urination. This is due to the inflammation and irritation of the urethra or vagina that's sometimes associated with chlamydia and other STDs. If untreated, STDs can lead to serious long-term problems, including pelvic inflammatory disease (PID) and infertility. Unlike UTIs, STDs are contagious.
            <h2>Symptoms of UTI</h2>
            A number of symptoms are associated with UTIs, including:
            <ul>
            <li>frequent urination</li>
            <li>burning or pain during urination</li>
            <li>Pain in the lower abdomen</li>
            <li>the feeling of having to pee even though little or no urine actually comes out</li>
            <li>pain above the pubic bone (in women)</li>
            <li>a full feeling in the rectum (in men)</li>
            <li>bloody or foul-smelling urine</li>
            <li>a general feeling of shakiness and fatigue</li>
            <li>mild fever</li>
            </ul>
            A kidney infection may involve more serious symptoms, including:
            <ul>
            <li>High fever</li>
            <li>Chills</li>
            <li>Nausea and vomiting</li>
            <li>Abdominal pain</li>
            <li>Cloudy or bloody urine</li>
            <li>Pain in the back, just above the waist</li>
            </ul>
            If you have any symptoms of a urinary tract infection, you'll need to go to a doctor right away. The symptoms won't go away if you ignore them — they'll only become worse. The more quickly you begin treatment, the less uncomfortable it will be.
            Call your doctor's office or clinic immediately. If you can't reach your doctor, you can visit an urgent care center or hospital emergency room. The most important thing is to take action as soon as possible.
            <br><span style="float: right!important"><i>For more Information call: 080062743786737</i></span>
            <h2>Preventing UTI</h2>
            There are several ways people may be able to prevent urinary tract infections. After urination, girls should wipe from front to back with toilet paper. After bowel movements, be sure to wipe from front to back to avoid spreading bacteria from the rectal area to the urethra.
              Another thing both girls and guys can do to prevent UTIs is to go to the bathroom frequently. Avoid holding urine for long periods of time.
              Males and females should also keep the genital area clean and dry. Girls should change their tampons and pads regularly during their periods. Frequent bubble baths can cause irritation of the vaginal area, so girls should take showers or plain baths. Avoid prolonged exposure to moisture in the genital area by not wearing nylon underwear or wet swimsuits. Wearing underwear with cotton crotches is also helpful. And girls should skip using feminine hygiene sprays or douches — these products can irritate the urethra.
              If you are sexually active, go to the bathroom both before and within 15 minutes after intercourse. After sex, gently wash the genital area to remove any bacteria. Avoid sexual positions that irritate or hurt the urethra or bladder. Couples who use lubrication during sex should use a water-soluble lubricant such as K-Y Jelly.
              Finally, drinking lots of water each day keeps the bladder active and bacteria free.
              Remember that although urinary tract infections are uncomfortable and often painful, they are very common and easily treated. The sooner you contact your doctor, the sooner you'll be able to get rid of the problem.
            `}
        ]
    },
    {id: 'Pregnancy prevention', data:[
          //{title:'Pregnancy', body:'Pregnancy, also known as gestation, is the time during which one or more offspring develops inside a woman. A multiple pregnancy involves more than one offspring, such as with twins. Pregnancy can occur by sexual intercourse or assisted reproductive technology. Childbirth typically occurs around 40 weeks from the last menstrual period (LMP). This is just over nine lunar months, where each month is about 29½ days. When measured from conception it is about 38 weeks. An embryo is the developing offspring during the first eight weeks following conception, after which, the term fetus is used until birth. Symptoms of early pregnancy may include missed periods, tender breasts, nausea and vomiting, hunger, and frequent urination. Pregnancy may be confirmed with a pregnancy test.'},
      {title: 'Pregnancy prevention', body: `Getting pregnant as a teen, adolescent or young adult when you are not ready can be pretty devastating socially, economically, physically and emotionally. This often occurs because of lack of access to correct information and service delivery points to help teens, adolescents and young adults make informed decisions and take charge of their bodies and health. 
Superb teens, adolescents and young adults use a pregnancy prevention method to take control of their life, future and dreams by avoiding getting pregnant when they are not fully ready. 
Starting a family or having a child is an important decision that changes one’s life forever and doing it at the right time can be the key to your happiness and success.
Pregnancy prevention method commonly known as contraceptive is ideal for you if you; Have had sex, do or don’t have children, are or are not married, want to get an education, space children or simply wants to slay!`},
      {title: 'Abstinence', body: `Abstinence from sex (oral, anal or vaginal) is the oldest means of pregnancy prevention and the only behavior that is 100 percent effective at preventing pregnancy.  Abstinence from sex is the best choice for teens, adolescents and young adults as it avoids early pregnancy, and sexually transmitted infections (STIs) among others although this can be difficult for some people and thus the need to protect self with a contraceptive choice.`},
      {title: 'Contraceptives',  body: `Besides abstinence, using a contraceptive method during sexual intercourse can effectively prevent pregnancy. There are several contraception methods currently available to suit your individual need. So whether you’re a slay queen, a glamour queen or a pepper them gang, you can conveniently find a contraceptive method that is right for you and can help to avoid pregnancy when you are not ready for the responsibility. These methods could be effective for one sexual episode, for months or years depending on your need.
            <h2><b>Types of contraceptive</b></h2>
            All contraceptive methods are safe for adoleesents as there are a varieties of methods to choose from. Temporary modern contraceptives:
            They are devices that can prevent pregnancy temporary. It could be effective for one sexual episode, for months, 3 months 3years depending on the type of contracetive used
            These methods are effective when correctly used
            The methods are reversible meaning they can be stoped when one is ready to get pregnant,
          <h2>Birth Control Pills</h2> There are two types of Birth Control Pills: Combined Oral Contraceptive Pills and Progestin-Only Pills. Combined oral contraceptive pills contain two hormones, an estrogen and a progestin. They work by stopping ovulation (release of an egg) and by inhibiting the movement of sperm. Among typical couples who initiate use of combined pills about eight percent of women will experience an accidental pregnancy in the first year. But if pills are used consistently and correctly, just three in 1,000 women will become pregnant. For increased protection against sexually transmitted diseases, use condoms as well. Pills alone do not protect against STIs and HIV.,
          <h2>Condoms</h2>Condoms act as a barrier between the genitalia to prevent exchange of fluids during sexual intercourse. They are available for both male and female packed colorfully with different flavor making every moment fun!   
whether you’re a glamour queen, a slay queen, pepper them gang or just confidently casual, condoms are a must have to match every girl’s style. They are the only birth control that protects against pregnancy and against STIs including HIV.
So be bold, take charge and accessorize with a condom every time you have sex.  For the best protection, use with one of the other four birth control options in case one breaks or isn’t used correctly.
            <h2>How it works</h2>
            A male condom is a layer of thin layer that goes over a guy’s erect penis and prevents sperm from entering the vagina.
A female condom is a loose plastic covering that is inserted into the vagina before sex. It also prevents sperm from entering the vagina but can be put in up to 6 hours before sex.
            <h2>Facts</h2>
            <ul>
              <li>it is easily available in all health centres listed in our contact</li>
              <li>Inexpensive and can be purchased easily</li>
              <li>Needs to be replaced each time you have sex</li>
              <li>Easy to carry but Can be easy for others to see or find if you’re not discrete</li>
              <li>No hormones, no side effect</li>
              <li>Partners might not like the feel thus Requires a conversation with your partner</li>
              <li>Can tear if not properly inserted thus dual protection is necessary to</li>
              <li>Comes in different colors and flavors</li>
            </ul>
            <h2>Male Condoms</h2>
            <ul>
              <li>It is made up of latext</li>
              <li>It prevents the ejaculated semen from entering the vagina</li>
              <li>It is applied on an erected pennis before sexual intercourse</li>
              <li>It must be used consistently and correctly to prevent pregnancy</li>
            </ul>
            <h2>Advantages</h2>
            <ul>
              <li>Easily available </li>
              <li>Inexpensive</li>
              <li>Usually no side effect</li>
              <li>Prevents HIV and other STIs in addition to pregnancy prevention</li>
            </ul>
            <h2>Diadvantages</h2>
            <ul>
                 <li>Can flip or tear if used incorrectly</li>
                 <li>Limits pleasures of sex</li>
                 <li>Partners might not like the feel</li>
            </ul>
            <h2>Female Condoms</h2>
            <ul>
              <li>It is a pouch made of polyurethane</li>
              <li>It covers the vagina and cervix</li>
              <li>It has two ends with rings; the inner smaller ring is closed and covers the cervix and the outer ring which is wider and open.</li>
              <li>Once inserted the outer ring lies outside the vagina</li>
              <li>During intercourse the penis is inserted into the vagina through the open end</li>
            </ul>
            <h2>Advantages</h2>
            <ul>
              <li>Usually no side effect</li>
              <li>Gives females more control in preventing pregnancy</li>
              <li>Prevents HIV and other STIs in addition to pregnancy prevention</li>
            </ul>
            <h2>Diadvantages</h2>
            <ul>
                 <li>Expensive</li>
                 <li>Not easily available</li>
            </ul>
            ,
          <h2>Oral Pills</h2>These are pills to b taken through the mouth by females
            It situable for females who have sexual intercourse on a regular basis
            It does not prevent HIV and other STIs
            Each tablest contains female hormones that thicken the cervix to prevent entry of sperm and fertilization
            e.g combination 3
            <h2>Facts</h2>
            <ul>
              <li>it is easily available in all health centres listed in our contact</li>
              <li>Only available for females, can be used discretely</li>
              <li>Can be stopped anytime and get pregnant quickly </li>
              <li>It is not expensive and Can be purchased at local pharmacies</li>
              <li>Regulates your period</li>
              <li>Can lessen cramps, lighten bleeding, clear up acne and protect against certain cancers</li>
              <li>Contains hormones rare side effects including weight changes or irregular periods which usually improve with time.</li>
              <li>It does not prevent against HIV and other STIs</li>
            </ul>
            <h2>Advantages</h2>
            <ul>
              <li>Easily available</li>
              <li>Inexpensive</li>
              <li>Females have control of when they want to get pregnant</li>
              <li>Regularize irregular menstration when prescribe by doctor</li>
            </ul>
            <h2>Disadvantages</h2>
            <ul>
              <li>It does not prevent HIV and other STIs</li>
              <li>Needs to be taken same tme every day which is really inconvenient for some</li>
              <li>Can have side effects such as nausea, irregular bleeding</li>
              <li>Regularize irregular menstration when prescribe by doctor</li>
              <li>If you are taking the pills and have this symptoms please kindly consult a doctor</li>
            </ul>,
          <h2>Cervical Cap</h2>The cervical cap is a small cap made of soft latex. A doctor or nurse practitioner "fits" a woman for a cervical cap. The woman puts spermicide (which destroys the sperm) in the cap and then places the cap up into her vagina and onto her cervix (the opening of the womb). Suction keeps the cap in place so sperm cannot enter the uterus (the womb). Women should obtain a new cap yearly. Among typical couples who initiate use of the cap before having a child, about 16 percent of women will experience an accidental pregnancy in the first year. If the cervical cap is used consistently and correctly, about nine percent of women will become pregnant. Failure rates are significantly higher if the cervical cap is used after a woman has had a child. Use a condom for additional protection against HIV and other STIs.`}
        ]
    },
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InformationPage');
  }

  itemSelected(id: number){
    this.navCtrl.push(InformationHubDetailPage, {data: this.getInformation(id)});
  }

  getInformation(id: number){
    return this.informations[id];
  }
}
