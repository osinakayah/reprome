import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UtiProvider} from "../../providers/uti/uti";
import {CallNumber} from "@ionic-native/call-number";
import {VisitCenterPage} from "../visit-center/visit-center";

/**
 * Generated class for the UtiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-uti',
  templateUrl: 'uti.html',
})
export class UtiPage {
  selectedContent = -1;
  info:any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private uti: UtiProvider,
              private call: CallNumber
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UtiPage');
    this.info = this.uti.getAllSTI();
  }

  toggleView(id:number){
    if(this.selectedContent == id){
      this.selectedContent = -1;
    }else{
      this.selectedContent = id;
    }
  }

  callNumber(){
    this.call.callNumber('22252', true);
  }
  visitCenter(){
    this.navCtrl.push(VisitCenterPage);
  }
}
