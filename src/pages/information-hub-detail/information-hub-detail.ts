import {Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ContraceptivePage} from "../contraceptive/contraceptive";
import {VisitCenterPage} from "../visit-center/visit-center";
import {CallNumber} from "@ionic-native/call-number";
import {StiPage} from "../sti/sti";
import {UtiPage} from "../uti/uti";
import {PtiPage} from "../pti/pti";

/**
 * Generated class for the InformationHubDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-information-hub-detail',
  templateUrl: 'information-hub-detail.html',
})
export class InformationHubDetailPage {
  trustedHtml = '';
  information: any;
  selectedContent = -1;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private call: CallNumber
  ) {
    this.information = navParams.get('data');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InformationHubDetailPage');
  }

  toggleView(id, title){
    if(title == 'Contraceptives'){
      this.navCtrl.push(ContraceptivePage);
      return;
    }
    else if(title == "STD's"){
      this.navCtrl.push(StiPage);
      return;
    }
    else if(title == "UTI"){
      this.navCtrl.push(UtiPage);
      return
    }
    else if(title == "PID"){
      this.navCtrl.push(PtiPage);
      return
    }
    if(this.selectedContent == id){
      this.selectedContent = -1;
    }else{
      this.selectedContent = id;
    }


  }

  callNumber(){
    this.call.callNumber('22252', true);
  }
  visitCenter(){
    this.navCtrl.push(VisitCenterPage);
  }
}
