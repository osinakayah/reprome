import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the StiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class StiProvider {
  sti:any = [];
  constructor(public http: Http) {
    this.sti = [
      {title:'How STD Spread', body:`STDs spread easily because you can't tell whether someone has an infection. In fact, some people with STDs don't even know that they have them. These people are in danger of passing an infection on to their sex partners without even realizing it.
          Some of the things that increase a person's chances of getting an STD are:
          <ul>
            <li><b>Sexual activity at a young age.</b> The younger a person starts having sex, the greater his or her chances of becoming infected with an STD</li>
            <li><b>Lots of sex partners.</b> People who have sexual contact — not just intercourse, but any form of intimate activity — with many different partners are more at risk than those who stay with the same partner.</li>
            <li><b>Unprotected sex.</b> Latex condoms are the only form of birth control that reduce your risk of getting an STD, and must be used every time. Spermicides, diaphragms, and other birth control methods may help prevent pregnancy, but they don't protect a person against STDs.</li>
          </ul>`
      },
      {title:'Common STD symptoms in women', body:`<ul>
            <li>No symptoms</li>
            <li>Discharge (thick or thin, milky white, yellow, or green leakage from the vagina) </li>
            <li>Vagina itching</li>
            <li>Vaginal blisters or blisters in the genital area (the region covered by underwear)</li>
            <li>area (the region covered by underwear)</li>
            <li>Burning urination</li>
            <li>Painful urination</li>
            <li>Pain during intercourse</li>
          </ul>`},
      {title:'Less Common STD symptoms in women', body:`<ul>
            <li>Bleeding or spotting between menstrual cycles</li>
            <li>Painless ulcers on the vagina</li>
            <li>Pelvic pain</li>
            <li>Lower back pain</li>
            <li>Fever</li>
            <li>Nausea</li>
            <li>Sore throat (after oral sex)</li>
            <li>Swelling of the joints (knee, elbow, etc.)</li>
            <li>Rectal pain, bleeding, or discharge (after receiving anal sex)</li>
          </ul>
          When symptoms do occur, they typically appear within days or weeks of exposure to an STD. Often, symptoms never appear or go unnoticed. Even if an infection never results in obvious symptoms, the STD can still be transmitted and progress into a more serious condition that may result in irreversible side effects. Regular comprehensive STD testing is the only way to guarantee a clean bill of sexual health. It is especially important to get tested for STDs after risky or unprotected sexual contact. 
          `},
      {title:'Common STD symptoms in men', body:`<ul>
            <li>Being asymptomatic or experiencing no symptoms at all</li>
            <li>Blisters on or around penis</li>
            <li>Stops, bumps or lesions on the penis</li>
            <li>Discharge (clear, white, or yellow)</li>
            <li>Oozing from the tip of the penis (thick or thin)</li>
            <li>Painful urination</li>
            <li>Painful ejaculation</li>
            <li>Itching on the tip of the penis</li>
            <li>Rash on the penis, testicles, or groin</li>
          </ul>`},
      {title:'Less common STD symptoms in men', body:`<ul>
            <li>Sore throat</li>
            <li>Fever</li>
            <li>Chronic flu-like symptoms</li>
            <li>Pain in the testicles</li>
            <li>Swelling of the testicles</li>
            <li>Swelling of the epididymis (known as Epididymitis)</li>
            <li>Swelling of the urethra (known as urethritis)</li>
            <li>Swelling of non-sexual joints (elbow, knee, etc.)</li>
            <li>Rectal pain, bleeding, or discharge (after receiving anal sex)</li>
          </ul>
          STD symptoms in men usually take a few days to develop, but can take up to weeks (if there are symptoms at all). A lack of symptoms is often mistaken for a lack of an STD, but an infection can continue to progress even in the absence of symptoms. Because men so often don’t show symptoms, the only way to be sure that an STD is not present is to get tested regularly, especially after unprotected sex. 
If you think you may have one or more of the symptoms or worried you might have been exposed to the infection or disthrough sex, please visit any of the health centres listed in our contact for comprehensive testing and treatment.
          `},
      {title:'Preventing and treating STDs', body:`As with many other diseases, prevention is key. It's much easier to prevent STDs than to treat them. The only way to completely prevent STDs is to abstain from all types of sexual contact. If someone is going to have sex, the best way to reduce the chance of getting an STD is by using a condom every time 
          People who have had or are having sex should get regular testing and screening examinations available at in all health centres listed in our contact. This is to help you get correct information on STDs and access services when they're detected in their earliest, most treatable stage.
          Don't let embarrassment at the thought of having an STD keep you from seeking medical attention or treatment as doing so may allow the disease to progress and cause more damage. Book appointments using the app appointment button to see a doctor If you think you may have an STD, or if you have had a partner who may have an STD, you should see a doctor right away.
          <h3>Common types of STDs common among teens, adolescent and young adults include:</h3>
          <hr>`},
      {title:'Chlamydia', body:`Chlamydia is a sexually transmitted disease (STD) caused by bacteria called Chlamydia trachomatis. Although you may not be familiar with its name, chlamydia is one of the most common STDs. Because there often aren't any symptoms, though, lots of people can have chlamydia and not know it. The bacteria can move from one person to another through vaginal, oral, or anal sex
          If you think you may have chlamydia or you might have been exposed to the bacteria through sex, please visit any of the health centres listed in our contact for comprehensive testing and treatment.  
          `},
      {title:'Hepatitis', body:`Hepatitis is a disease of the liver. It is usually caused by a virus, but also can be caused by long-term overuse of alcohol or other toxins (poisons).
          There are several different types of hepatitis. Hepatitis B is a type that can move from one person to another through blood and other body fluids. People also can get it through having sex or from needles that haven't been properly sterilized. 
          If you think you may have hepatitis B or you might have been exposed to the virus through sex or any other means visit any of the health centres listed in our contact for comprehensive testing and treatment. The good news is HB can be treated completely.
          <h2>Gonorrhea</h2>
          Gonorrhea is a sexually transmitted disease caused by bacteria. The bacteria can be passed from one person to another through vaginal, oral, or anal sex, even when the person who is infected has no symptoms.
          If you think you may have hepatitis B or you might have been exposed to the bacteria through sex or any other means visit any of the health centres listed in our contact for comprehensive testing and treatment. The good news is HB can be treated completely
          <hr>`},
      {title:'HIV', body:`The human immunodeficiency virus (HIV) is one of the most serious, deadly diseases in human history. HIV causes a condition called acquired immunodeficiency syndrome better known as AIDS.
          Thousands of young adults get infected with HIV each year. HIV can be transmitted from an infected person to another person through body fluids like blood, semen, vaginal fluids, and breast milk.
          The virus is spread through things like:
           <ul>
            <li>having unprotected oral, vaginal, or anal sex ("unprotected" means not using a condom)</li>
            <li>sharing needles, such as needles used to inject drugs, steroids, and other substances, or sharing needles used for tattooing including other risk factors</li>
           </ul>
           `}
    ];
  }
  getOneSTI(index){
    return this.sti[index];
  }
  getAllSTI(){
    return this.sti;
  }
}
