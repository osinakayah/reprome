import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the UtiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UtiProvider {
  information:any = [];
  constructor(public http: Http) {
    console.log('Hello UtiProvider Provider');
    this.information = [
      {title: 'UTI', body:`A bacterial urinary tract infection (UTI) is the most common kind of infection affecting the urinary tract. Urine, or pee, is the fluid that is filtered out of the bloodstream by the kidneys. Urine contains salts and waste products, but it doesn't normally contain bacteria. When bacteria get into the bladder or kidney and multiply in the urine, a UTI can result.
            There are three main types of UTI. Bacteria that infect only the urethra (the short tube that delivers urine from the bladder to the outside of the body) cause urethritis (pronounced: yur-ih-threye-tis).
            Bacteria can also cause a bladder infection, which is called cystitis (pronounced: sis-tie-tis). Another, more serious, kind of UTI is infection of the kidney itself, known as pyelonephritis (pronounced: pie-low-nih-fry-tis). With this type of UTI, a person often has back pain, high fever, and vomiting.The most common type of UTI, the bladder infection, causes mostly just discomfort and inconvenience. Bladder infections can be quickly and easily treated. And it's important to get treatment promptly to avoid the more serious infection that reaches the kidneys.
            `},
      {title:'What Causes UTI', body:`UTIs are usually caused by E. coli, bacteria that are normally found in the digestive tract and on the skin around the rectal and vaginal areas. When the bacteria enter the urethra, they can make their way up into the bladder and cause an infection.
            Girls get urinary tract infections much more frequently than guys, most likely due to differences in the shape and length of the urethra. Girls have shorter urethras than guys, and the opening lies closer to the rectum and vagina where bacteria are likely to be.
            Bacteria can get into the urethra several ways. During sexual intercourse, for example, the bacteria in the vaginal area may be pushed into the urethra and eventually end up in the bladder, where urine provides a good environment for the bacteria to grow. This is the reason why females who are sexually active often get UTIs (UTIs are not contagious, so you can't catch a urinary tract infection from someone else).
            Bacteria may also be introduced into a girl's bladder by wiping from back to front after a bowel movement, which can contaminate the urethral opening. The use of spermicides (including condoms treated with spermicide) and diaphragms as contraceptives also may increase the risk of UTIs.
            Sexually transmitted diseases (STDs) may cause UTI-like symptoms, such as pain with urination. This is due to the inflammation and irritation of the urethra or vagina that's sometimes associated with chlamydia and other STDs. If untreated, STDs can lead to serious long-term problems, including pelvic inflammatory disease (PID) and infertility. Unlike UTIs, STDs are contagious.
            `},
      {title:'Symptoms of UTI', body:`A number of symptoms are associated with UTIs, including:
            <ul>
            <li>frequent urination</li>
            <li>burning or pain during urination</li>
            <li>Pain in the lower abdomen</li>
            <li>the feeling of having to pee even though little or no urine actually comes out</li>
            <li>pain above the pubic bone (in women)</li>
            <li>a full feeling in the rectum (in men)</li>
            <li>bloody or foul-smelling urine</li>
            <li>a general feeling of shakiness and fatigue</li>
            <li>mild fever</li>
            </ul>
            A kidney infection may involve more serious symptoms, including:
            <ul>
            <li>High fever</li>
            <li>Chills</li>
            <li>Nausea and vomiting</li>
            <li>Abdominal pain</li>
            <li>Cloudy or bloody urine</li>
            <li>Pain in the back, just above the waist</li>
            </ul>
            If you have any symptoms of a urinary tract infection, you'll need to go to a doctor right away. The symptoms won't go away if you ignore them — they'll only become worse. The more quickly you begin treatment, the less uncomfortable it will be.
            Call your doctor's office or clinic immediately. If you can't reach your doctor, you can visit an urgent care center or hospital emergency room. The most important thing is to take action as soon as possible.
            <br><span style="float: right!important"><i>For more Information call: 080062743786737</i></span>
            `},
      {title:'Preventing UTI', body:`There are several ways people may be able to prevent urinary tract infections. After urination, girls should wipe from front to back with toilet paper. After bowel movements, be sure to wipe from front to back to avoid spreading bacteria from the rectal area to the urethra.
              Another thing both girls and guys can do to prevent UTIs is to go to the bathroom frequently. Avoid holding urine for long periods of time.
              Males and females should also keep the genital area clean and dry. Girls should change their tampons and pads regularly during their periods. Frequent bubble baths can cause irritation of the vaginal area, so girls should take showers or plain baths. Avoid prolonged exposure to moisture in the genital area by not wearing nylon underwear or wet swimsuits. Wearing underwear with cotton crotches is also helpful. And girls should skip using feminine hygiene sprays or douches — these products can irritate the urethra.
              If you are sexually active, go to the bathroom both before and within 15 minutes after intercourse. After sex, gently wash the genital area to remove any bacteria. Avoid sexual positions that irritate or hurt the urethra or bladder. Couples who use lubrication during sex should use a water-soluble lubricant such as K-Y Jelly.
              Finally, drinking lots of water each day keeps the bladder active and bacteria free.
              Remember that although urinary tract infections are uncomfortable and often painful, they are very common and easily treated. The sooner you contact your doctor, the sooner you'll be able to get rid of the problem.
            `}
    ];
  }

  public getOneUTI(index:number){
    return this.information[index];
  }

  public getAllSTI(){
    return this.information;
  }

}
