import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the PtiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PtiProvider {
  info:any = [];
  constructor(public http: Http) {
    console.log('Hello PtiProvider Provider');
    this.info = [
      {title:'What are the symtoms PID', body:`
        PID can cause severe symptoms or very mild to no symptoms. Girls who do have symptoms may notice:
             <ul>
               <l1>Pain and tenderness in the lower abdomen</l1>
               <li>Bad-smelling or abnormally colored discharge</li>
               <li>Pain during sex</li>
               <li>Spotting (small amounts of bleeding) between periods</li>
               <li>Chills or fever</li>
               <li>nausea, vomiting, or diarrhea</li>
               <li>loss of appetite</li>
               <li>backache and perhaps even difficulty walking</li>
               <li>pain while peeing or peeing more often than usual</li>
               <li>pain in the upper abdomen on the right</li>
             </ul>
      `}
    ];
  }
  getPDI(){
    return this.info;
  }

}
