import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AuthProvider {
  private BASE_URL:string = 'http://repromestat.com.ng/';
  constructor(public http: Http) {
  }

  register(name: string, email: string, password: string){
    let registerPromise = new Promise((resolve, reject)=>{
      this.http.post(this.BASE_URL+'api/register', {name:name, email:email, password:password})
        .map(res=>res.json())
        .subscribe(res=>{
          resolve(res);
        });
    });
    return registerPromise;
  }

  login(email: string, password: string){
    let loginPromise = new Promise((resolve, reject)=>{
      this.http.post(this.BASE_URL+'api/login', {email:email, password:password})
        .map(res=>res.json())
        .subscribe(res=>{
          resolve(res);
        });
    });
    return loginPromise;
  }

  sendFeedBack(){

  }
  sendReommendation(name: string, message: string){
    let recommendationPromise = new Promise((resolve, reject)=>{
      this.http.post(this.BASE_URL+'api/recommendation', {name:name, message:message})
        .map(res=>res.json())
        .subscribe(res=>{
          resolve(res);
        });
    });
    return recommendationPromise;
  }


}
